const fs = require('fs');
const csv = require('@fast-csv/parse');
const path = require('path');

function getGladiatorsByYear(req, res) {
  const year = parseInt(req.params.year);
  const GLADIATORS_CSV_PATH = 'gladiators.csv';

  try {
    const result = [];
    const gladiators = fs.createReadStream(path.resolve(GLADIATORS_CSV_PATH))
    .pipe(csv.parse({ headers: true }))
    .on('data', (row) => {
      if (year >= parseInt(row['first year']) && year <= parseInt(row['last year'])) {
        result.push(row);
      }
    })
    .on('end', () => {
      res.json(result);
    })
    .on('error', (error) => {
      throw error;
    });
  } catch (err) {
    console.error(err.stack)
    res.status(500).send('Internal Server Error');
  }
}

module.exports = {
  getGladiatorsByYear
};
