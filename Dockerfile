FROM node:latest
WORKDIR /app
COPY . .
RUN npm ci --only-production
EXPOSE 4200
ENV PORT 4200
ENTRYPOINT ["node", "./bin/www"]